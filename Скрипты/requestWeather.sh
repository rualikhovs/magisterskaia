#!/bin/bash
KEY_UTC='utc_time'
KEY_EPOCH='mills'
KEY_NUM='num'
FILE="history.json"
TMP="tmp.json"

API_KEY='fc2d1ed30b8b2d8ff9563aed6b9e4551'
CITY_ID=520555

REQUEST="api.openweathermap.org/data/2.5/weather?id=$CITY_ID&APPID=$API_KEY"
DATE_UTC="$(date)"
TIME_SECS="$(date +%s)"
TIME_HOURS="$((${TIME_SECS}/3600))"
RESPONSE="$(curl ${REQUEST})"
TIMED_RESPONSE="$(echo ${RESPONSE} | sed '$s/{/{\"time_s\":'"${TIME_SECS}"',/')"
DATED_RESPONSE="$(echo ${TIMED_RESPONSE} | sed '$s/{/{\"date\":\"'"${DATE_UTC}"'\",/')"
SOURCE="$(cat ${FILE})"
RESULT="${SOURCE::-1}"",""${DATED_RESPONSE}""]"
echo ${RESULT} | jq '.' > ${FILE}
LENGTH="$(echo ${RESULT} | jq '. | length')"
LENGTH="$((${LENGTH}-1))"
echo ${RESULT} | jq '.['"${LENGTH}"'].date'
echo "done"
echo "Use 'git add -u && git commit --message='<your message>'' to commit"
cp $FILE ../Эксперимент
