const UTF_8 = "UTF-8";
const OWM_API_ID = "42feb0ab2a38c117ddf379d7e9ee3e95";
const SMS_API_ID = "195E9CCE-FE80-13BA-A94D-8D44DECD7674";

const OWM_KEY_CITY_ID = "id=";
const OWM_KEY_APP_ID = "APPID=";
const OWM_CITY_ID = "520555";// N.Novgorog
const OWM_BASE_URL = "http://api.openweathermap.org/data/2.5/weather?";

const SMS_KEY_APP_ID = "api_id=";
const SMS_KEY_TO = "to=";
const SMS_TO = "79036004349";
const SMS_KEY_TEXT = "text=";
const SMS_TEXT = "Отчёт+сформирован";
const SMS_BASE_URL = "http://sms.ru/sms/send?";

const FILE_NAME = "data.json";
const KEY_DATA_TEMP = "temp";
const KEY_DATA_PRESS = "press";

var temperatures = new Array();
var pressures = new Array();

var artPressure = [];

/**
 * Эта функция выполняется при загрузке окна - некоторое подобие main() в Си
 */
window.onload = function () {
  var drawingCanvas = document.getElementById('maincanvas');
  if (drawingCanvas && drawingCanvas.getContext) {
    var context = drawingCanvas.getContext('2d');
    drawGraph(context);
  }
  document.getElementById('loaddata').addEventListener('change', openFile, false);
}

function generateXs(range, amount) {
  let pxls = [];
  const step = range / amount;
  for (let i = 0, current = 0; i < amount; i++) {
    pxls.push(current);
    current += step;
  }
  return pxls;
}

function drawGraph(context) {
  let canvas = document.getElementById('maincanvas');
  context.clearRect(0, 0, canvas.width, canvas.height);//очистка экрана перед внесением данных
  if (temperatures.length == 0) {
    console.log("Сведения о температуре не загружены");
  } else {
    let gg = new GraphGenerator();
    gg.setPoints(generateXs(600, temperatures.length), temperatures);
    gg.constructor(context, canvas.width, canvas.height, 0.1);
    gg.drawGraph('red');
    context.font = "20px Arial";
    context.fillStyle = 'red'
    context.textAlign = 'left';
    //отступ 1/40 слева и 1/10 сверху + 20 пикселей, чтобы не перекрывать "Давление"
    context.fillText("Температура", canvas.width / 40, canvas.height / 10 + 20);
  }
  if (pressures.length == 0) {
    console.log("Сведения о давлении не загружены");
  } else {
    let gg = new GraphGenerator();
    gg.setPoints(generateXs(600, pressures.length), pressures);
    gg.constructor(context, canvas.width, canvas.height, 0.1);
    gg.drawGraph('blue');
    context.font = "20px Arial";
    context.fillStyle = 'blue'
    context.textAlign = 'left';
    //отступ 1/40 слева и 1/10 сверху
    context.fillText("Давление", canvas.width / 40, canvas.height / 10);
  }
  if (pressures.length == 0 && temperatures.length == 0) {//Данных нет
    context.font = "30px Arial";
    context.fillStyle = 'grey'
    context.textAlign = 'center';
    context.fillText("Данных нет", canvas.width / 2, canvas.height / 2 - 15);
    context.font = "15px Arial";
    context.fillText("загрузите данные, нажав Обзор...", canvas.width / 2, canvas.height / 2 + 15);
  }
}

/**
 * Класс генератора графиков
 */
function GraphGenerator() {
  this.context = null;//контекст - по нему происходит привязка рисования к нужному элементу canvas
  this.width = 0;//ширина
  this.height = 0;//высота
  this.pointX = new Array();//набор точек X
  this.pointY = new Array();//набор точек Y
  this.field = 0.1;//отступ от краёв

  this.constructor = function (context, width, height, field) {
    this.context = context;
    this.width = width;
    this.height = height;
    this.field = field;
  }

  this.setPoints = function (pointX, pointY) {
    if (pointX.length == pointY.length) {
      this.pointX = pointX;
      this.pointY = pointY;
      return 0;
    } else {
      return 1;
    }
  }

  this.drawPolyLine = function (x, y) {
    let i = 0;
    this.context.font = "15px Arial";
    this.context.fillStyle = 'grey'
    this.context.textAlign = 'center';
    this.context.beginPath();
    for (i = 0; i < x.length && i < y.length; i++) {
      if (this.context.strokeStyle == '#ff0000') {//Адовый костыль - по цвету контекста определяем тип данных :D 
        this.context.fillText(temperatures[i], x[i], y[i] - 15);
      } else {
        this.context.fillText(pressures[i], x[i], y[i] - 15);
      }
      this.context.lineTo(x[i], y[i]);
    }
    this.context.stroke();
  }

  this.drawGraph = function (color) {
    if (null == this.pointX || null == this.pointY)//один из массивов не задан
      return 1;
    if (this.pointX.length != this.pointY.length)//длина массивов не совпаает
      return 2;
    let length = this.pointY.length;
    let pointYint = new Array();
    let pointXint = new Array();

    let min = Math.min.apply(null, this.pointY);

    let ampY = this.getAmp(this.pointY);
    let ampX = this.getAmp(this.pointX);
    let aY = (this.height * (1 - this.field * 2)) / ampY;
    let aX = (this.width * (1 - this.field * 2)) / ampX;
    let bY = Math.floor(this.height * (1 - this.field));
    let bX = Math.floor(this.width * this.field);
    let i;
    for (i = 0; i < length; i++) {
      pointYint[i] = Math.floor(-aY * (this.pointY[i] - min)) + bY;
      pointXint[i] = Math.floor(aX * this.pointX[i]) + bX;
    }
    this.context.lineWidth = 6;//6 пикселей - ширина линий
    this.context.strokeStyle = color;
    this.drawPolyLine(pointXint, pointYint);
    return 0;
  }

  this.getAmp = function (src) {
    return (Math.max.apply(null, src) - Math.min.apply(null, src));
  }

  this.drawLineWithComment = function (line, offsetX, offsetY) {
    ig2.setPaint(line.color);
    ig2.setStroke(new BasicStroke(line.width));
    ig2.drawLine(line.xBegin, line.yBegin, line.xEnd, line.yEnd);
    let commentX = line.xEnd + offsetX;
    let commentY = line.yEnd + offsetY;
    this.context.font = "30px Arial";
    this.context.strokeText(line.comment, commentX, commentY);
    return 0;
  }
}

/**
 * Обработчик на событие выбора файла с данными (кнопка "Обзор...")
 */
var openFile = function (event) {
  if (event != undefined) {
    let input = event.target;
    let reader = new FileReader();
    //коллбек, выполяемый по завершении загрузки файла с локальной файловой системы
    reader.onload = function () {
      let data = reader.result;
      let jsonFileData = JSON.parse(data);
      //fillYcoords(jsonFileData);
      //printWeather(jsonFileData);
      printSelector();
      printTable(jsonFileData);
      drawGraph(document.getElementById('maincanvas').getContext('2d'));
    };
    reader.readAsText(input.files[0]);
  }
};

/**
 * Заполняет координаты по Y для температуры и давления
 */
function fillYcoords(jsonFileData) {
  for (let i = 0; i < jsonFileData.length; i++) {
    temperatures[i] = jsonFileData[i].main.temp;
    pressures[i] = jsonFileData[i].main.pressure;
  }
}

/*
 * выполняет запрос по URL
 */
function httpGet(theUrl) {
  let xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", theUrl, false); // false for synchronous request
  xmlHttp.send(null);
  return xmlHttp.responseText;
}

/**
 * Запросить свежие данные о погоде
 */
function getWeather() {
  console.log("Запрос погоды...")
  let request = OWM_BASE_URL.concat(OWM_KEY_APP_ID, OWM_API_ID, "&", OWM_KEY_CITY_ID, OWM_CITY_ID);
  let response = httpGet(request);
  console.log(response);
  let jsonResponse = JSON.parse(response);
  console.log(jsonResponse.main.temp);
  console.log(jsonResponse.main.pressure);
}

function printSelector() {
  const values = [
    { id: 'dt', key: 'dt', header: 'Timestamp' },
    { id: 'temp', key: 'main.temp', header: 'Temperature' },
    { id: 'pressure', key: 'main.pressure', header: 'Pressure' },
    { id: 'humidity', key: 'main.humidity', header: 'Humidity' },
    { id: 'wind', key: 'wind.speed', header: 'Wind' },
    { id: 'date', key: 'date', header: 'Date' }
  ]
  const ul_begin = '<ul>', ul_end = '</ul>';
  const li_begin = '<li class=\"select\">', li_end = '</li>';
  const arr = values.map(value => {
    return '<input id=\"' + value.id + '\" name=\"' + value.header + '\" type="checkbox"> \
      <label for=\"'+ value.id + '\">' + value.header + '</label>'
  });
  //let checkboxDiv = document.getElementById('checkbox');
  //checkboxDiv.innerHTML = ul_begin + li_begin + arr + li_end + ul_end;
}

function toggleCheckbox(element) {
  console.log("checkbox clicked");
  element.checked = !element.checked;
}

function printTable(data) {
  if (!data || data.length == 0) {
    return;
  }
  let tableDiv = document.getElementById('tableData');
  const beginTag = "<table class='main-table'>";
  const endTag = "</table>";
  const header = "<tr> \
    <th>Дата</th> \
    <th>Температура</th> \
    <th>Давление</th> \
    <th>Влажность</th> \
    <th>Ветер</th> \
    </tr>"
  let table = beginTag + header;
  let i;
  for (i = 0; i < data.length; i++) {
    let bgstyle = i % 2 == 0 ? 'row-grey' : 'row-white';

    const timestamp = "<td class='main-td " + bgstyle + "'>" + data[i].dt + "</td>";
    const temp = "<td class='main-td " + bgstyle + "' >" + data[i].main.temp + "</td>";
    const press = "<td class='main-td " + bgstyle + "'>" + data[i].main.pressure + "</td>";
    const humidity = "<td class='main-td " + bgstyle + "'>" + data[i].main.humidity + "</td>";
    const wind = "<td class='main-td " + bgstyle + "'>" + data[i].wind.speed + "</td>";
    const date = "<td class='main-td " + bgstyle + "'>" + data[i].date + "</td>";
    const row = "<tr class='main-td " + bgstyle + "'>" + timestamp + temp + press + humidity + wind + "</tr>";
    table += row;
  }
  table += "<tr><td colspan = 5>" + i +" rows</td></tr>"
  table += endTag;
  tableDiv.innerHTML = table;
}

function printWeather(data) {
  if (!data || data.length == 0) {
    return;
  }
  //console.log("Printing " + data.length + " elements");
  for (let i = 0; i < data.length; i++) {
    const timestamp = data[i].dt;
    const temp = data[i].main.temp;
    const press = data[i].main.pressure;
    const humidity = data[i].main.humidity;
    const wind = data[i].wind.speed;
    console.log("time: " + timestamp + ", Temp:" + temp + ", Press:" + press + ", Hum:" + humidity + ", Wind:" + wind);
  }
}

/**
 * Отправить SMS
 */
function sendSMS() {
  console.log("Отправка SMS...")
  //let request = SMS_BASE_URL.concat(SMS_KEY_APP_ID, SMS_API_ID, "&", SMS_KEY_TO, SMS_TO, "&", SMS_KEY_TEXT, SMS_TEXT);
  //let response = httpGet(request);
  //console.log(response);
}
